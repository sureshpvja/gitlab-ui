const PROXY_CONFIG = [
  {
    "context": [
      "**"
    ],
    "target": "https://gitlab.com",
    "secure": true,
    "changeOrigin": true,
    "logLevel": "debug"
  }
]

module.exports = PROXY_CONFIG;
