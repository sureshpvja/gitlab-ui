import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition, group} from '@angular/animations';

/**
 * Component to show loading skeleton indicators
 */
@Component({
  selector: 'app-skeleton-loading',
  templateUrl: './skeleton-loading.component.html',
  styleUrls: ['./skeleton-loading.component.scss'],
  animations: [
    trigger('fadeInOut', [
        transition(':enter', [
          style({opacity: 0}),
          animate('250ms ease-out', style({opacity: 1}))
        ]),
        transition(':leave', [
          animate('250ms ease-in', style({opacity: 0}))
        ])
      ]),
      trigger('fadeOutSkeleton', [
        transition(':leave', [
          animate('250ms ease-in', style({opacity: 0}))
        ])
      ]),
      trigger('fadeContent', [
        state('out', style({
          height: 0,
          opacity: 0,
          overflow: 'hidden'
        })),
        state('in', style({
          opacity: 1,
          height: '*'
        })),
        transition('out => in', group([
          animate('250ms 250ms ease-out', style({
            opacity: 1
          })),
          animate('1ms 250ms linear', style({
            height: '*'
          }))
        ]))
      ])
]
})
export class SkeletonLoadingComponent implements OnInit {
  /**
   * Property to show/hide loading skeleton
  */
  @Input() isLoading = false;
  /**
   * Property to show/hide overlay
   */
  @Input() useOverlay = false;
  /**
   * Property to show/hide spinner
   */
  @Input() useSpinner = false;

  constructor() { }

  ngOnInit(): void {
  }

}
