export interface ProjectDetails {
  id: string;
  description: string;
  name: string;
  avatar_url: string;
}

export interface RepositoryFiles {
  id: string;
  name: string;
  type: string;
  path: string;
}
