import { TestBed } from '@angular/core/testing';

import { ProjectDetailsAPI } from './projectDetailsAPI';

describe('ProjectDetailsService', () => {
  let service: ProjectDetailsAPI;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectDetailsAPI);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
