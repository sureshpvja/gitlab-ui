import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectDetails, RepositoryFiles } from '../models';

/**
 * Service to handle Gitlab API's
 */
@Injectable({
  providedIn: 'root'
})
export class ProjectDetailsAPI {

  constructor(private httpClient: HttpClient) { }

  getProjectDetails(projectId: string): Observable<ProjectDetails> {
    return this.httpClient.get<ProjectDetails>(`/api/v4/projects/${projectId}`);
  }

  getRepositoryFiles(projectId: string): Observable<RepositoryFiles[]> {
    return this.httpClient.get<RepositoryFiles[]>(`/api/v4/projects/${projectId}/repository/tree?per_page=200`)
  }
}
