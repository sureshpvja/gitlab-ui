import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProjectDetails, RepositoryFiles } from '../data/models';
import { HomeService } from './service/home.service';

/**
 * Home component that shows the project details and files
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public projectId = "";
  public projectDetails!: ProjectDetails;
  public repositoryFiles!: RepositoryFiles[];
  public isLoading = false;

  constructor(private homeService: HomeService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  getProjectDetails() {
    this.isLoading = true;
    this.homeService.getProjectDetails(this.projectId).subscribe({
      next: (response) => {
        this.isLoading = false;
        this.projectDetails = response[0];
        this.repositoryFiles = response[1];
      },
      error: (error) => {
        this.isLoading = false;
        this.snackBar.open(error.message, "", {
          horizontalPosition: "center",
          verticalPosition: "top",
        });
      }
    });
  }

}
