import { Component, Input, OnInit } from '@angular/core';
import { ProjectDetails } from 'src/app/data/models';

/**
 * Component that shows the Gitlab project name and description
 */
@Component({
  selector: 'app-repository-header',
  templateUrl: './repository-header.component.html',
  styleUrls: ['./repository-header.component.scss']
})
export class RepositoryHeaderComponent implements OnInit {
  /**
   * Project details with name and description
   */
  @Input() public projectDetails!: ProjectDetails;

  constructor() { }

  ngOnInit(): void {
  }

}
