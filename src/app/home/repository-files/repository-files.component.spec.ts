import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepositoryFilesComponent } from './repository-files.component';

describe('RepositoryFilesComponent', () => {
  let component: RepositoryFilesComponent;
  let fixture: ComponentFixture<RepositoryFilesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepositoryFilesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepositoryFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
