import { Component, Input, OnInit } from '@angular/core';
import { RepositoryFiles } from 'src/app/data/models';

/**
 * Component that shows files of Gitlab repository
 */
@Component({
  selector: 'app-repository-files',
  templateUrl: './repository-files.component.html',
  styleUrls: ['./repository-files.component.scss']
})
export class RepositoryFilesComponent implements OnInit {
  /**
   * List of repository files
   */
  @Input() repositoryFiles: RepositoryFiles[] = [];
  /**
   * Loading indicator
   */
  @Input() isLoading = true;
  public displayedColumns: string[] = ['name'];

  constructor() { }

  ngOnInit(): void {
  }

}
