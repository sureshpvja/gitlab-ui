import { Injectable } from '@angular/core';
import { forkJoin, map, Observable } from 'rxjs';
import { ProjectDetails, RepositoryFiles } from 'src/app/data/models';
import { ProjectDetailsAPI } from 'src/app/data/project-details/projectDetailsAPI';

/**
 * Service that handles HomeComponent api requests
 */
@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private projectDetailsAPI: ProjectDetailsAPI) { }

  getProjectDetails(projectID: string): Observable<[ProjectDetails, RepositoryFiles[]]> {
    return forkJoin(
      [
        this.projectDetailsAPI.getProjectDetails(projectID),
        this.projectDetailsAPI.getRepositoryFiles(projectID)
      ]
    );
  }
}
